# Followage.
Show how long someone has been following the channel, can be the user sending the command or another user.

##### Example:
```
!addcom !followage /me $(urlfetch https://2g.be/twitch/following.php?user=$(touser)&channel=$(channel)&format=mwdhms)
```

##### Result:
```
5c0tt1: !followage
Nightbot:
5c0tt1: !followage @AnotherUser
Nightbot:
```