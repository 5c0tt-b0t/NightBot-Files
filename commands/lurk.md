# Lurk.
Declare yourself as AFK.

##### Example:
```
!addcom !lurk Thanks for lurking $(user)! <3
```
##### Result:
```
5c0tt1: !lurk
Nightbot: Thanks for lurking @5c0tt1 <3
```