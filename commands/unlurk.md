# Unlurk.
Declare that you have returned again.

##### Example:
```
!addcom !unlurk Welcome back $(user)! <3
```

##### Result:
```
5c0tt1: !unlurk
Nightbot: Welcome back @5c0tt1! <3
```