# Time.
Show the streamers time, tags the user or another user.

##### Example:
Change the location to streamers location! find them all here: [Nightbot location variables](https://nightbot.com/variables/locations)
```
!addcom !time $(touser) the time for $(channel) is: $(time America/Los_Angeles "h:mm a z (MMMM Do)").
````

##### Result:
```
5c0tt1: !time
Nightbot: @5c0tt1 the time for Shroud is: 9:55PM PST (Tuesday 21/10).
5c0tt1: !time @AnotherUser
Nightbot: @AnotherUser the time for Shroud is: 9:55PM PST (Tuesday 21/10).
```