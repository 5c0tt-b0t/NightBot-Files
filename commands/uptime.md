# Uptime.
Show how long the channel has been live for, it will tag the user sending the command or another user.

##### Example:
```
!addcom !uptime $(touser) - $(channel) has been live for: $(twitch $(channel) "{{uptimeLength}}").
```

##### Result:
```
5c0tt1: !uptime
Nightbot: @5c0tt - Shroud has been live for: 9 hours, 47 minutes & 32 seconds.
5c0tt1: !uptime @AnotherUser
Nightbot: @AnotherUser - Shroud has been live for: 9 hours, 47 minutes & 32 seconds.
```