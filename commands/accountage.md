# Accountage.
Show how long someone has been following the channel, can be either the user sending the command or another user.

##### Example:
```
!addcom !accountage $(touser) - (urlfetch https://decapi.me/twitch/accountage/$(touser))
```

##### Result:
```
5c0tt1: !accountage
Nightbot:
5c0tt1: !accountage @AnotherUser
Nightbot:
```