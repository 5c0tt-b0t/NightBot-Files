# NightBot Commands.

Whilst some are basic and can be found from NightBot's website, others are alot more advanced. For example, some commands can be rigged, others return different replies depending a how high a user is rated, any command that could give a negative response to the streamer (like !fight, !potato, !insult etc) will be positive and a lot more too.

Before adding these commands, I suggest you read these tips first:
[Nightbot Notes](https://codeberg.org/5c0tt-b0t/NightBot-Files/src/commit/29d2553a9606bfdae8c2a9e32d402e27cf83e8af/nightbot-notes.md)

## Command list:

| Command | Description |
| --- | --- |
| Lurk | Declare yourself AFK |
| Unlurk | State you have returned |
| Uptime | Show how long the streamer has been live |
| AccountAge | Show a users account age  |
| FollowAge | Show how long a user has followed the channel |
| ShoutOut | Promote another user |
| [Time](https://codeberg.org/5c0tt-b0t/NightBot-Files/src/commit/bd9f29651aa34192730e5ad04cca265f6c67d545/commands/time.md) | Show the streamers time |
| TimeSearch | Show the time for a location |
| Weather | Show the streamers weather |
| WeatherSearch | Show the weather for a location |
| Hug | Hug a user |
| Love | Rate how much love two users have |
| Slap | Slap a user with random items |
| Fight | Fight another user, see who wins |
| 8ball | Ask the magic 8ball for an answer to a question |
| Fortune | Open a fortune cookie to find out a users fortune |
| CoinFlip | Flip a coin, heads or tails |
| Pick | Pick from a list of choices |
| Advice | Quotes to success from DJ Khaled |
| Hotness | Rate how hot a user is |
| Bot | Rate how bot a user is |
| Potato | Rate how potato a user is |
| VibeCheck | Rate a users vibes |
| Ban | Fake ban a user |
| Report | Fake report a user |
| Insult | Insult a user |
| FU | "Fuck you" translated into any language |
| ChuckNorris | A random Chuck Norris fact |
| DogFact | A random dog fact |
| CatFact| A random cat fact |
| Quote | Quote someone or something |
| Quotes | Show quotes (search or random) |
| Mock | Mock what someone said |
| Stats | Show channel stats |
| Stream | Show stream stats |
| Multi | Show the multistream URL |
| MultiSet | Set the multistream URL |
| MultiClear | Reset the multistream URL |
| Xmas | Show the countdown till christmas |
| Birthday | Show the date & countdown till streamers birthday |
| Emote spam | Multiple emotes with optional text that is centralised  |
|||
| Emotes | Remind users that this channel uses FFZ & BTTV emotes. Also posts the URLs to add them |
| Twitter | Posts streamer Twitter account URL |
| Tweet | Post a URL to the streamers latest tweet |
| TweetStream | A URL so users can tweet your stream on Twitter |
| TweetSearch | Search for any Twitter users latest tweet |
| Youtube | Posts streamers Youtube URL |
| YtNew | Posts streamers latest Youtube Video |
| Discord | Posts streamers Discord server invite |
| Instagram | Posts streamers Instagram URL |
| Clips | Random Twitch clip (or search) |
| LMGTFY | Let Me Google That For You |
| Drop | Picks a random place to drop on the map (WarZone, Apex, PUBG & Fortnite) |