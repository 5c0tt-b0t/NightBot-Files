# Custom commands.
## Basic custom commands:
**Lurk** (declare that you are AFK).

`
!addcom !lurk Thanks for the lurk $(user)! <3
`

**Unlurk** (declare you are back).

`
!addcom !unlurk Welcome back $(user)! <3
`

**Uptime** (show how long the streamer has been live).

`
!addcom !uptime $(touser) Shroud has been live for: $(twitch $(channel) "{{uptimeLength}}")
`

**Shoutout** (give another user a shoutout, I suggest making userlevel moderator or vip).

`
`

**Followage** (show how long a user has followed).

`
!addcom !followage $(urlfetch https://2g.be/twitch/following.php?user=$(touser)&channel=$(channel)&format=mwdhms)
`

**Accountage** (show how long someone has been on Twitch).

`
!addcom !accountage $(touser) - (urlfetch https://decapi.me/twitch/accountage/$(touser))
`

**Time** (shows the streamers time).

`
!addcom !time $(touser) The time for Shroud is: $(time America/Los_Angeles "h:mm a z (MMMM Do)")
`

**Weather** (shows the streamers weather).

`
!addcom !weather $(touser) streamers weather is $(weather London)
`

**WeatherSearch** (shows the weather anywhere).

`
!addcom !weathersearch $(weather $(query))
`

**Hug** (hug another user).

`
!addcom !hug $(user) gives $(touser) a hug! <3
`

**Slap** (basic version: slap another user).

`
!addcom !slap $(user) just slapped $(touser)!
`

**Advice** (random tips to success from DJ Khaled).

`
!addcom !advice $(djkhaled)
`

**Xmas** (the amount of time until christmas).

`
!addcom !xmas $(touser) - There are $(countdown dec 25 2021 12:00:00 AM America/Los_Angeles) till Christmas!!!
`

**Birthday** (the amount time until the streamers birthday).

`
!addcom !birthday $(touser) - There are $(countdown mar 17 2021 12:00:00 AM America/Los_Angeles) until 5c0tt1's birthday!
`

## Advanced custom commands:
**Slap** (multiple options).

```
!addcom !slap $(user) slapped $(touser) with $(eval a=[`a backhand!`,`their glove!`,`a hotdog!`];a[Math.floor(Math.random()*a.length)])!
```

**Hotness** (rate hotness out of 10).

```
!addcom !hotness $(touser), on a scale of 1 to 10, you’re like a $(eval Math.floor((Math.random() * 10) + 1)).
```

**Pick** (from multiple options, any amount separated by commas).

```
!addcom !pick $(touser) the chosen one is...... $(eval const a=`$(query)`.split(',');a[Math.floor(Math.random()*a.length)]).
```

**CoinFlip** (flip a coin).

```
!addcom !coinflip $(touser) flips a coin and it lands on $(eval ['Heads.','Tails.'][Math.floor(Math.random()*2)])!
```

**8ball** (ask it a question, get an answer).

```
!addcom !8ball $(touser), $(eval const responses = ['All signs point to yes...', 'Yes!', 'My sources say nope.', 'You may rely on it.', 'Yup!', '100%!', 'I better not tell you.', 'Very doubtful.', 'Definitely!', 'It is certain!', 'Most likely.', 'Probably', 'Ask again later.', 'No!', 'No chance!', 'No way!', 'Outlook is good.', 'You don\'t wanna know!', 'Don\'t count on it.']; responses[Math.floor(Math.random() * responses.length)];)
```

**Quote** ().

```
!addcom !quote $(eval q=decodeURIComponent(`$(querystring)`).split(` `); u=q.shift().replace(`@`,``); `Once upon a time, a wise person once said: "${q.join(` `)}." - ${u}. CoolStoryBob`)
```

**Ban** (fake ban a user).

```
!addcom !ban $(eval q=decodeURIComponent(`$(querystring)`).split(` `); u=q.shift().replace(`@`,``); `The user "${u}" has been banned for: "${q.join(` `)}." Jebaited`)
```

**Ban** (also give a reason).

```
```

**Emote spam** (with optional text in the middle).

```
!addcom !gasm $(eval q=decodeURIComponent(`$(querystring)`).split(` `); `Kreygasm Kreygasm Kreygasm Kreygasm ${q.join(` `)} Kreygasm Kreygasm Kreygasm Kreygasm`)
```

**Love** (show love between the  sender & a tagged user).
```

!addcom !love There is a $(customapi http://2g.be/twitch/randomnumber.php?=defstart=1&defend=100)% chance of love between $(user) and $(touser).
```

**Tweet** (get the streamers latest tweet).

```
!addcom !tweet New tweet from @5c0tt1! $(customapi https://decapi.me/twitter/latest.php?name=**TwitterNameGoesHere**&no_rts)

```

**TweetSearch** (find any Twitter users latest tweet).

```
!addcom !tweetsearch Latest tweet from $(query): $(customapi https://decapi.me/twitter/latest.php?name=$(query)&no_rts)

```

**TweetMe** (provides a link for a user that will tweet your stream on twitter).

```

```
**Respond to a word in chat** (if the first word is "what" and the sentence contains "your id" it will post your iD).

```
!addcom what $(eval `$(query)`.toLowerCase().includes("your id") ? "My iD is BlahBlahBlah" : " ")
```

**Respond to multiple words in chat** (if first word is "what" and sentence contains "your steam" it will respond with your Steam iD, but if the sentence contained "your activision" instead, it would respond with your Activision iD).

`
`

**Vibes** (a vibe-check out of ten, replies different depending on a pass/fail).

`
`