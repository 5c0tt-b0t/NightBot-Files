Useful notes to remember when adding custom commands via Twitch chat.

All the basics are here:
- **Default commands.**
https://docs.nightbot.tv/control-panel/commands
- **variables** (to use in custom commands).
https://docs.nightbot.tv/commands/variableslist
- **Dashboard** to view all of your custom commands (you can add commands here too).
https://nightbot.tv/commands/custom

----

| Syntax | Meaning | Values |
| --- | --- | --- |
| `-a=` | Alias | Another **command name**. |
| `-ul=` | User Level | **moderator**, **vip**, **regular**, **subscriber**, **owner** or **everyone**. Default value is: **everyone**. |
| `-cd=` | Cooldown | A **number** of seconds (Can't be less than: **5**). Default value is: **10**. |
| `/me` | Colored Text | Will always be the color of Nightbot's name in chat, which can be changed. |

### Examples

1. `
!addcom !coloredText -ul=moderator -cd=10 /me This is some colored text.
`

Would add command `!coloredText`, that outputs colored text, used by **moderators only** with a cooldown of **10 seconds**.

2. `
!addcom !aliasCommand -a=!anotherCommand $(query)
`

Would alias new command `!aliasCommand` so it responds the same as `!anotherCommand`.

You can also edit existing commands as shown above, just replace `!addcom` with `!editcom`.

### Save time, add these aliases!

Add commands with `!ac`:
```
!addcom !ac -a=!addcom $(query)
```

Edit commands with `!ec`:
```
!addcom !ec -a=!editcom $(query)
```

Delete commands with `!dc`:
```
!addcom !dc -a=!delcom $(query)
```
**Thank me later ;)**

----

### Command types
Nightbot responds to **anything** you want, not just `!exampleCommand` etc, it also responds to:
- Words
`!addcom hey`
- Names
`!addcom 5c0tt1`
- Emotes
`!addcom Kappa`
- URL links
`!addcom https://twitch.tv/5c0tt1`
- Tagging someone
`!addcom @5c0tt1`
- Any prefix symbol
`!addcom ?commandName`
